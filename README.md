## Purpose

Simple application providing always-on webhooks for tests and demo purposes

## Architecture

The application follows a microservice architecture, it is composed of: 

- www: the frontend web
- api: microservice which manipulates the data and communicate with an underlying database
- ws: websocket server to make sure the frontend is updated in real time each time the application receives a new payload

On top of those micro-services, additional components are used: 

- NATS message broker
- MongoDB database
- Traefik reverse proxy in front of the application

The schema below illustrates the architecture of the application: 

![Architecture](./images/architecture.png)

## Running the application locally with Docker Compose

- Clone the application repositories

```
mkdir webhooks && cd webhooks
for repo in www api wss web-hook; do
  git clone https://gitlab.com/web-hook/$repo.git
done
```

- Run the application with Docker Compose:

```
cd web-hook
docker compose up
```

The application will be available on http://localhost

## Running the application on a local Kubernetes cluster

- Creation of a local k3s cluster

First create a VM using Mumtipass

```
multipass launch --name webhooks --cpus 4 --mem 4G --disk 10G
```

Next run a shell in that VM:

```
multipass shell webhooks
```

Next install k3s inside of it:

```
curl -sfL https://get.k3s.io | sh -s
```

Next get the kubeconfig created:

```
multipass exec webhooks -- sudo cat /etc/rancher/k3s/k3s.yaml > config
IP=$(multipass info webhooks | grep IP | awk '{print $2}')
sed -i.bak "s/127.0.0.1/$IP/" config
```

Next configure your local *kubectl*:

```
export KUBECONFIG=$PWD/config
```

Next make sure you can see the single node from your local machine:

```
kubectl get no
```

Then install the application using Helmfile running the following command in the *config/apps/webhooks* folder:

```
helmfile apply
```

## Usage

First go to [https://webhooks.app](https://webhooks.app)

![Home](./images/wh-1.png)

You will directly get the URL and token of your dedicated webhook

Next go to the dashboard (will be empty as no data have been sent to your webhook yet)

![Dashboard](./images/wh-2.png)

Then send a test payload from the dedicated button

![Test payload](./images/wh-3.png)

You can also send a test payload from a terminal using the *cURL* example

![cURL examples](./images/wh-4.png)

## Development

The following instructions clone the whole app and run it in development mode with Docker Compose:

```
mkdir webhooks && cd webhooks
for repo in www api wss web-hook; do
  git clone https://gitlab.com/web-hook/$repo.git
done
```

- Using docker compose

```
cd web-hook
docker compose -f compose.dev.yml up
```

The whole app is available on [http://localhost](http://localhost)

- Using Acorn

Note: you need to have a Kubernetes cluster with Acorn running inside of it first

```
cp web-hook/Acornfile .
acorn run -i .
```

The application will be available on the domain provided by Acorn

In this mode, the changes done in the local IDE will be taken into account in real time in the application running in containers.

